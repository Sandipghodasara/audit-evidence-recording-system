<?php
$page = "remarks";
?>

<!-- Header section including sidebar -->
<?php
include "header.php";
?>

<div class="md:max-w-6xl md:mx-auto px-4 py-8">
	<?php
	include "flash_msg.php"
	?>

<?php
$sql = "SELECT count(*) as count FROM remarks where status = 1";
$result = $conn->query($sql); 

foreach ($result as $key => $value) {	
	if ($value['count'] > 0) {
		?>
		<div class="flex items-center justify-between mb-4">
			<h2 class="text-xl font-bold text-gray-800">REQUESTED REMARKS</h2>
		</div>
		
		<div class="overflow-x-auto bg-white rounded-lg shadow mb-8">
			<table id="myTable" class="w-full whitespace-no-wrap bg-white overflow-hidden table-striped">
				<thead>
					<tr class="text-left">
						<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">ID</th>
						<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Remarks</th>
						<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "select * from remarks where status = 1";
					$result = $conn->query($sql);
		
		
					foreach ($result as $key => $record) {
					?>
						<tr class="focus-within:bg-gray-200 overflow-hidden">
							<td class="border-t">
								<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $key + 1 ?></span>
							</td>
							<td class="border-t">
								<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $record['remark'] ?></span>
							</td>
							<td class="border-t">
								<a href="<?php echo "req_remarks.php?remarks_id=" . $record['id'].'&action=approve' ?>" class="shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
									Approv
								</a>
								<a href="<?php echo "req_remarks.php?remarks_id=" . $record['id'].'&action=rejected' ?>" class="shadow inline-flex items-center bg-red-500 hover:bg-red-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
									Rejected
								</a>
							</td>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<?php
	}
}
?>

	<div class="flex items-center justify-between mb-4">
		<h2 class="text-xl font-bold text-gray-800">REMARKS</h2>

		<a href="./form_remarks.php" class="shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-white font-semibold py-2 px-4 rounded-lg">
			<svg xmlns="http://www.w3.org/2000/svg" class="mr-2 w-5 h-5" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
				<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
				<line x1="12" y1="5" x2="12" y2="19" />
				<line x1="5" y1="12" x2="19" y2="12" />
			</svg>
			Create Remarks
		</a>
	</div>

	<div class="overflow-x-auto bg-white rounded-lg shadow">
		<table id="myTable" class="w-full whitespace-no-wrap bg-white overflow-hidden table-striped">
			<thead>
				<tr class="text-left">
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">ID</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Remarks</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$store_id = isset($_SESSION['store_id']) ? $_SESSION['store_id'] : '';
				$sql = "select * from remarks where store_id = $store_id and status = 0 ORDER BY id DESC";
				$result = $conn->query($sql);


				foreach ($result as $key => $record) {
					$id = $record['id'];

					$temp_id = ($id)*987654321;
					$encode_id = urlencode(base64_encode($temp_id));
				?>
					<tr class="focus-within:bg-gray-200 overflow-hidden">
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $key + 1 ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $record['remark'] ?></span>
						</td>
						<td class="border-t">
							<a href="<?php echo "form_remarks.php?remarks_id=" . $encode_id ?>" class="shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
								Edit
							</a>
							<a href="<?php echo "deleteRemarkSQL.php?remarks_id=" . $encode_id ?>" class="shadow inline-flex items-center bg-red-500 hover:bg-red-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
								Delete
							</a>
						</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>


<!-- footer section -->
<?php
include "footer.php";
?>