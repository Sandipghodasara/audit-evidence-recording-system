<?php

session_start();

require_once "dbConnection.php";

function valid()
  {
    if (empty($_POST["remark"])) {
        $flashMsg["message"][] = "remark is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $remark_valid = false;
    } else {
        $remark_valid = true;
    }
    
    $_SESSION['flash_message_data'] = $flashMsg;

    return $remark_valid;
}

if ($_POST && valid() && isset($_POST['submit'])) {
    $current_remarks_id = $_POST['remarks_id'];
    $remark = $_POST['remark'];

    $sql = "UPDATE remarks SET  remark='$remark' WHERE id = $current_remarks_id";
    
    $flashMsg = [];
    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>remarks</span> record Updated succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }
    $_SESSION['flash_message_data'] = $flashMsg;
    header("Location:list_remarks.php");

}else{
    $current_remarks_id = $_POST['remarks_id'];
    header("Location:form_remarks.php?remark_id=$current_remarks_id");
}

?>