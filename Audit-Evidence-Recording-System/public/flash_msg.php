<?php

$flash_message_data = isset($_SESSION['flash_message_data']) ? $_SESSION['flash_message_data'] : null;
if (!empty($flash_message_data) && !empty($flash_message_data['color'])) {
    $hover_color = $flash_message_data['color'] === "bg-green-200 text-green-700" ? "bg-green-100" : "bg-red-100";
}
unset($_SESSION['flash_message_data']);


if (!empty($flash_message_data && is_array($flash_message_data['message']))) {
    foreach ($flash_message_data['message'] as $key => $value) {
?>
    <div class="<?php echo !empty($flash_message_data) ? $flash_message_data['color'] : '' ?> px-6 py-4 rounded-lg relative mb-5" role="alert" x-data="{ open: <?php echo !empty($flash_message_data) ? "true" : "false" ?> }" x-show.transition="open">
        <div class="mr-4">
            <span class="block sm:inline"><?php echo !empty($flash_message_data) ? $value : '' ?></span>
        </div>

        <span class="cursor-pointer absolute top-0 bottom-0 right-0 hover:<?php echo $hover_color ?> hover: <?php echo !empty($flash_message_data) ? $flash_message_data['color'] : '' ?>  text-red-600 w-10 h-10 rounded-full inline-flex items-center justify-center mt-2 mr-3" x-on:click="open = false">
            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <rect x="0" y="0" width="24" height="24" stroke="none"></rect>
                <line x1="18" y1="6" x2="6" y2="18" />
                <line x1="6" y1="6" x2="18" y2="18" />
            </svg>
        </span>
    </div>
    <?php
    }
} else {
    ?>
    <div class="<?php echo !empty($flash_message_data) ? $flash_message_data['color'] : '' ?> px-6 py-4 rounded-lg relative mb-5" role="alert" x-data="{ open: <?php echo !empty($flash_message_data) ? "true" : "false" ?> }" x-show.transition="open">
        <div class="mr-4">
            <span class="block sm:inline"><?php echo !empty($flash_message_data) ? ($flash_message_data['message']) : '' ?></span>
        </div>

        <span class="cursor-pointer absolute top-0 bottom-0 right-0 hover:<?php echo $hover_color ?> hover: <?php echo !empty($flash_message_data) ? $flash_message_data['color'] : '' ?>  text-red-600 w-10 h-10 rounded-full inline-flex items-center justify-center mt-2 mr-3" x-on:click="open = false">
            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <rect x="0" y="0" width="24" height="24" stroke="none"></rect>
                <line x1="18" y1="6" x2="6" y2="18" />
                <line x1="6" y1="6" x2="18" y2="18" />
            </svg>
        </span>
    </div>
<?php
}
?>


<!-- <div class="<?php echo !empty($flash_message_data) ? $flash_message_data['color'] : '' ?> px-6 py-4 rounded-lg relative mb-5" role="alert" x-data="{ open: <?php echo !empty($flash_message_data) ? "true" : "false" ?> }" x-show.transition="open">
    <div class="mr-4">
        <span class="block sm:inline"><?php echo !empty($flash_message_data) ? $flash_message_data['message'] : '' ?></span>
    </div>

    <span class="cursor-pointer absolute top-0 bottom-0 right-0 hover:<?php echo $hover_color ?> hover: <?php echo !empty($flash_message_data) ? $flash_message_data['color'] : '' ?>  text-red-600 w-10 h-10 rounded-full inline-flex items-center justify-center mt-2 mr-3" x-on:click="open = false">
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <rect x="0" y="0" width="24" height="24" stroke="none"></rect>
            <line x1="18" y1="6" x2="6" y2="18" />
            <line x1="6" y1="6" x2="18" y2="18" />
        </svg>
    </span>
</div> -->