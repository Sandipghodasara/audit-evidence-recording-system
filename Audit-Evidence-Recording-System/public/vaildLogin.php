<?php

  session_start();
  if(isset($_SESSION["userName"])){
    if ($_SESSION['role'] == 1) {
      header("location:dashbord.php");
    }else{
      header("location:dashbord_user.php");
    }
  }

  require "dbConnection.php";

  function valid()
  {
    if (empty($_POST["email"])) {
      $_SESSION['error_data']['email'] = "Email is required";
    } else {
      $email_valid = true;
    }

    if (empty($_POST["password"])) {
      $_SESSION['error_data']['password'] = "Password is required";
    } else {
      $password_valid = true;
    }

    return $email_valid && $password_valid;
  }
  
  if($_POST && valid()){
    $sql = "Select * from users";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
      while($row = $result->fetch_array()){
        if($_POST['email'] === $row['email'] && $_POST['password'] === $row['password']){
          $_SESSION['userName'] = substr($row['first_name'],0,1).substr($row['last_name'],0,1);
          $_SESSION['role'] = $row['role'];
          $_SESSION['user_id'] = $row['id'];
          if ($row['role'] == 1) {
            echo $row['role'];
            header("location:dashbord.php");
          }else{
            header("location:dashbord_user.php");
          }
        }else{
          $_SESSION['error'] = "User or Password is incorrect";
          header("location:index.php");
        }
      }
    }
  }else{
    header("location:index.php");
  }

?>
