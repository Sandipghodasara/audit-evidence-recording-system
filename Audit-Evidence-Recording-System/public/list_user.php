<?php
$page = "listUser";
?>

<div id="hidden" class="view_model hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center" id="modal-id">
	<div class="relative w-auto my-6 mx-auto max-w-3xl">
		<!--content-->
		<div class="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
			<!--header-->
			<!--body-->
			<div class="relative p-6 flex-auto">
				<button id="close_view_model" class="focus:outline-none cursor-pointer absolute top-0 bottom-0 right-0 hover:bg-gray-100 text-black w-10 h-10 rounded-full inline-flex items-center justify-center mt-2 mr-3" x-on:click="open = false" type="button">
					<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
						<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
						<line x1="18" y1="6" x2="6" y2="18" />
						<line x1="6" y1="6" x2="18" y2="18" />
					</svg>
				</button>
				<table id="view_model" class="w-full whitespace-no-wrap bg-white overflow-hidden table-striped">
					<thead>
						<tr class="text-left">
							<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">ID</th>
							<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Store name</th>
							<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">City</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="hidden1" class="view_model1 hidden opacity-25 fixed inset-0 z-40 bg-black" id="modal-id-backdrop"></div>

<!-- Header section including sidebar -->
<?php
include "header.php";
?>

<div class="md:max-w-6xl md:mx-auto px-4 py-8">
	<?php
	include "flash_msg.php"
	?>

	<div class="flex items-center justify-between mb-4">
		<h2 class="text-xl font-bold text-gray-800">USERS</h2>

		<a href="./form_user.php" class="shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-white font-semibold py-2 px-4 rounded-lg">
			<svg xmlns="http://www.w3.org/2000/svg" class="mr-2 w-5 h-5" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
				<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
				<line x1="12" y1="5" x2="12" y2="19" />
				<line x1="5" y1="12" x2="19" y2="12" />
			</svg>
			Create User
		</a>
	</div>

	<div class="overflow-x-auto bg-white rounded-lg shadow">
		<table id="myTable" class="w-full whitespace-no-wrap bg-white overflow-hidden table-striped">
			<thead>
				<tr class="text-left">
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">ID</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">First Name</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Last Name</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Stores</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sql = "select * from users where role=2 ORDER BY id DESC";
				$result = $conn->query($sql);


				foreach ($result as $key => $record) {
					$id = $record['id'];

					$temp_id = ($id) * 987654321;
					$encode_id = urlencode(base64_encode($temp_id));

				?>
					<tr class="focus-within:bg-gray-200 overflow-hidden">
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $key + 1 ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $record['first_name'] ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $record['last_name'] ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center">
								<?php
								$sql = "SELECT count(*) as count FROM users_stores JOIN stores on stores.id = users_stores.store_id WHERE user_id = $id";
								$result = $conn->query($sql);

								foreach ($result as $key => $value) {
									echo $value['count'];
								}
								?>
							</span>
						</td>
						<td class="border-t">
							<a href="<?php echo "form_user.php?user_id=" . $encode_id ?>" class="shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
								Edit
							</a>
							<a href="<?php echo "deleteUserSQL.php?user_id=" . $encode_id ?>" class="shadow inline-flex items-center bg-red-500 hover:bg-red-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
								Delete
							</a>
							<button id="abcd" data-val="<?php echo $encode_id ?>" data-person="user" class="shadow inline-flex items-center bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
								Show
							</button>
						</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>


<!-- footer section -->
<?php
include "footer.php";
?>