<?php
session_start();

$user_not_Valid = isset($_SESSION['error']) ? $_SESSION['error'] : null;
$error = isset($_SESSION['error_data']) ? $_SESSION['error_data'] : null;
unset($_SESSION['error']);
unset($_SESSION['error_data']);

if(isset($_SESSION["userName"])){
  if ($_SESSION['role'] == 1) {
    header("location:dashbord.php");
  }else{
    header("location:dashbord_user.php");
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="tailwind.css">
    <script src="ap"></script>
    <title>Audit Evidence Recording System</title>
</head>
<body class="flex h-screen bg-gray-50">
    
    <div class="w-full max-w-xs m-auto bg-indigo-100 rounded-lg p-5 shadow-xl">   
        <!-- header -->
        <header>
            <img class="w-20 mx-auto mb-5" src="https://img.icons8.com/fluent/344/year-of-tiger.png" />
        </header>

        <!-- form -->
        <form action="./vaildLogin.php" method="POST">
            <div>
             <label class="block mb-2 text-indigo-500" for="email">Email</label>
             <input class="w-full p-2 mb-1 text-indigo-700 border-b-2 <?php echo !empty($error['email']) ? 'border-red-500' : 'border-indigo-500' ?> outline-none focus:bg-gray-100" type="email" name="email">
             <span class="text-red-700 text-sm"><?php echo !empty($error['email']) ? $error['email'] : '' ?></span>
           </div>
           <div>
             <label class="block mb-2 text-indigo-500" for="password">Password</label>
             <input class="w-full p-2 mb-1 text-indigo-700 border-b-2 <?php echo !empty($error['password']) ? 'border-red-500' : 'border-indigo-500' ?> outline-none focus:bg-gray-300" type="password" name="password">
             <span class="text-red-700 text-sm"><?php echo !empty($error['password']) ? $error['password'] : '' ?></span>
           </div>
           <div>          
             <!-- <input  type="submit"> -->
             <button class="w-full bg-indigo-700 hover:bg-pink-700 text-white text-center font-bold py-2 px-4 my-4 rounded" type="submit" name="login">login</button>
           </div>
         </form>

        <!-- footer -->
        <footer class="text-center">
          <span class="text-red-700 font-center"><?php echo !empty($user_not_Valid) ? $user_not_Valid : '' ?></span>
        </footer> 

    </div>
    
</body>
</html>