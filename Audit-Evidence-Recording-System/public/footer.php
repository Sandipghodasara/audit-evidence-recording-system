</div>
</div>

<script>
	function dataFileDnD() {
		return {
			files: [],
			fileDragging: null,
			fileDropping: null,
			humanFileSize(size) {
				const i = Math.floor(Math.log(size) / Math.log(1024));
				return (
					(size / Math.pow(1024, i)).toFixed(2) * 1 +
					" " + ["B", "kB", "MB", "GB", "TB"][i]
				);
			},
			remove(index) {
				let files = [...this.files];
				files.splice(index, 1);

				this.files = createFileList(files);
			},
			drop(e) {
				let removed, add;
				let files = [...this.files];

				removed = files.splice(this.fileDragging, 1);
				files.splice(this.fileDropping, 0, ...removed);

				this.files = createFileList(files);

				this.fileDropping = null;
				this.fileDragging = null;
			},
			dragenter(e) {
				let targetElem = e.target.closest("[draggable]");

				this.fileDropping = targetElem.getAttribute("data-index");
			},
			dragstart(e) {
				this.fileDragging = e.target
					.closest("[draggable]")
					.getAttribute("data-index");
				e.dataTransfer.effectAllowed = "move";
			},
			loadFile(file) {
				const preview = document.querySelectorAll(".preview");
				const blobUrl = URL.createObjectURL(file);

				preview.forEach(elem => {
					elem.onload = () => {
						URL.revokeObjectURL(elem.src); // free memory
					};
				});

				return blobUrl;
			},
			addFiles(e, type = "dropbox") {
				console.log(e);
				const files = createFileList([...this.files], [...e.target.files]);
				this.files = files;
				this.form.formData.files = [...files];
			}
		};
	}

	$(document).ready(function() {
		$('#set_time').timepicker();
		$('#set_time').timepicker({
			'timeFormat': 'H:i:s'
		});
		$('#set_time_button').on('click', function() {
			$('#set_time').timepicker('setTime', new Date());
		});

		$('[data-toggle="datepicker"]').datepicker({
			format: 'dd-mm-yyyy',
			autoHide: true,
			// autoPick: true,
		});

		$('[data-toggle="datepicker1"]').datepicker({
			format: 'dd-mm-yyyy',
			autoPick: true,
			autoHide: true,
		});

		$('#set_date_button').on('click', function() {
			$('[data-toggle="datepicker1"]').datepicker({
				format: 'dd-mm-yyyy',
				autoPick: true,
				autoHide: true,
			});
		});

		$('#startDate').datepicker({
			format: 'dd-mm-yyyy',
			autoHide: true,
		});

		$('#endDate').datepicker({
			format: 'dd-mm-yyyy',
			filter: date => {
				var split = ($('#startDate').val()).split("-");
				var join = `${split[2]}-${split[1]}-${split[0]}`
				return (new Date(join) >= date) ? false : true;
			},
			autoHide: true,
		});

		$('#remarks').on('change', function() {
			var selected = $(this).children('option:selected').val()
			if (selected === 'custom') {
				$('.custom_remarks').removeClass('invisible').addClass('visible');
			} else {
				$('.custom_remarks').removeClass('visible').addClass('invisible');
			}
		});

		$("#aa").on("change", function() {
			console.log("in");
			var storeId = $(this).find('option:selected').val();
			$.ajax({
				type: "POST",
				url: 'view_audit_report.php', // change url as your 
				data: {
					'store_id': storeId,
					'method': 'store'
				},
				dataType: 'json',
				success: function(res) {
					if (res.status === true && res.action === 'store') {
						location.reload();
					}
				}
			});
		})

		$("#btn_filter").on("click", function() {
			var storeId = $('#aa').find('option:selected').val();
			var startDate = $('#startDate').val().split("-");
			startDate = `${startDate[2]}-${startDate[1]}-${startDate[0]}`
			var endDate = $('#endDate').val().split("-");
			endDate = `${endDate[2]}-${endDate[1]}-${endDate[0]}`
			console.log(startDate, endDate);
			$.ajax({
				type: "POST",
				url: 'view_audit_report.php', // change url as your 
				data: {
					'startDate': startDate,
					'endDate': endDate,
					'method': 'filter'
				},
				dataType: 'json',
				success: function(res) {
					if (res.status === true && res.action === 'filter') {
						location.reload();
					}
				}
			});
		})

		$("#ref").on("click", function() {
			$("#btn_filter").trigger("click");
		})


		const fileInput = document.getElementById("myupload");

		window.addEventListener('paste', e => {
			fileInput.files = e.clipboardData.files;
			var clone = $(".img_clone").eq(0)
			clone = clone.removeClass("hidden").addClass("current");
			// clone = clone.find("button").addClass("btn_delete");
			// console.log(clone);

			function loadFile(file) {
				const preview = document.querySelectorAll(".preview");
				const blobUrl = URL.createObjectURL(file);

				preview.forEach(elem => {
					elem.onload = () => {
						URL.revokeObjectURL(elem.src); // free memory
					};
				});
				return blobUrl;
			}

			function humanFileSize(size) {
				const i = Math.floor(Math.log(size) / Math.log(1024));
				return (
					(size / Math.pow(1024, i)).toFixed(2) * 1 +
					" " + ["B", "kB", "MB", "GB", "TB"][i]
				);
			}

			var img = loadFile(fileInput.files[0])
			var name = e.clipboardData.files[0].name
			var size = humanFileSize(fileInput.files[0].size)
			$(clone).find('img').attr("src", img)
			$(clone).find('.file_name').html(name)
			$(clone).find('.file_size').html(size)


			$(".img_template").before().append(clone)

		});

		$(".btn_delete").on("click", function() {
			$(".img_template").find(".img_clone").addClass("hidden")
		})

		$("td").on("click", function() {
			var imgURL = $(this).find("img").attr("src")
			$("#img_model").attr("src", imgURL)
		})

		$("td > #abcd").on("click", function() {
			var userId = $(this).attr("data-val")
			var method = $(this).attr("data-person")
			$.ajax({
				type: "POST",
				url: 'showUser.php', // change url as your 
				data: {
					'user_id': userId,
					'method': method
				},
				dataType: 'json',
				success: function(res) {
					if (res.status == true) {
						$("#hidden").removeClass("hidden");
						$("#hidden1").removeClass("hidden");
						console.log(res);
						var method = res.method == "user" ? res.store_name.length : res.first_name.length;
						console.log(method);
						for (let i = 0; i < method; i++) {
							var td2 = res.method == "user" ? res.store_name[i] : res.first_name[i];
							var td3 = res.method == "user" ? res.city[i] : res.last_name[i];
							var abc = `<tr class="focus-within:bg-gray-200 overflow-hidden">
								<td class="border-t">
									<span class="text-gray-700 px-6 py-4 flex items-center">${i+1}</span>
								</td>
								<td class="border-t">
									<span class="text-gray-700 px-6 py-4 flex items-center">${td2}</span>
								</td>
								<td class="border-t">
									<span class="text-gray-700 px-6 py-4 flex items-center">${td3}</span>
								</td>
							</tr>`
							$("#view_model").find("tbody").append(abc)
						}
					}
				}
			});
		})

		$("#close_view_model").on("click", function(){
			$("#view_model").find("tbody").empty()
			$(".view_model").addClass("hidden");
			$(".view_model1").addClass("hidden");
		})

		$(".js-example-basic-multiple").select2();

		$('#myTable').DataTable();
		$('#myTable_wrapper').addClass('p-4');
		$('#myTable').addClass('pt-3');
		$('#myTable_info').addClass('mt-2');
		$('#myTable_paginate').addClass('mt-2');
		$('#myTable_filter').find('input').addClass('w-30 pl-10 px-4 py-1.5 rounded-lg shadow focus:outline-none focus:shadow-outline');
	})
</script>

</body>

</html>