<?php

session_start();

require_once "dbConnection.php";

function valid()
  {
    if (empty($_POST["remark"])) {
        $flashMsg["message"][] = "remark is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $remark_valid = false;
    } else {
        $remark_valid = true;
    }
    
    $_SESSION['flash_message_data'] = $flashMsg;

    return $remark_valid;
}

if ($_POST && valid() && isset($_POST['submit'])) {
    $remark = $_POST['remark'];
    $store_id = $_SESSION['store_id'];
    
    $sql = "INSERT INTO remarks (store_id, remark)
    VALUES ('$store_id', '$remark')";

    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>audit</span> created succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }
    $_SESSION['flash_message_data'] = $flashMsg;
    header("Location:list_remarks.php");
}else{
    header("Location:form_remarks.php");
}



?>