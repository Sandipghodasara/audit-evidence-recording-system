<?php

session_start();

require_once "dbConnection.php";

function valid()
  {
    if (empty($_POST["first_name"])) {
        $flashMsg["message"][] = "First name is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $first_name_valid = false;
    } else {
        $first_name_valid = true;
    }
    
    if (empty($_POST["last_name"])) {
        $flashMsg["message"][] = "Last name is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $last_name_valid = false;
    } else {
        $last_name_valid = true;
    }

    if (empty($_POST["email_address"])) {
        $flashMsg["message"][] = "Email is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $email_valid = false;
    } else {
        $email_valid = true;
    }

    if (!empty($_POST["password"]) && !empty($_POST["confirm_password"])) {
        if(strlen($_POST["password"]) >=6 && strlen($_POST["password"]) <= 10){
            if ($_POST["password"] != $_POST["confirm_password"]) {
                $flashMsg["message"][] = "The password and confirmation password do not match";
                $flashMsg["color"] = "bg-red-200 text-red-700";
                $password_valid = false;
            }else{
                $password_valid = true;
            }
        }else{
            $flashMsg["message"][] = "password length should be between 6 and 10";
            $flashMsg["color"] = "bg-red-200 text-red-700";
            $password_valid = false;
        }
    } else {
        if (empty($_POST["password"])){
            $flashMsg["message"][] = "password is required";
        }else{
            $flashMsg["message"][] = "confirm password is required";
        }
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $password_valid = false;
    }

    $_SESSION['flash_message_data'] = $flashMsg;

    return $email_valid && $first_name_valid && $last_name_valid && $password_valid;
}

if ($_POST && valid() &&isset($_POST['submit'])) {
    $fanme = $_POST['first_name'];
    $lanme = $_POST['last_name'];
    $email = $_POST['email_address'];
    $password = $_POST['password'];
    $dob = $_POST['dob'];
    $store_id = $_POST['selected'];

    $role = "2";

    $sql = "INSERT INTO users (first_name, last_name, email, password, dob, role)
    VALUES ('$fanme', '$lanme', '$email', '$password', '$dob', '$role')";

    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>user</span> record created succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }

    $current_user_id_sql = "SELECT id FROM `users` WHERE first_name = '$fanme' AND email= '$email'";
    $result = $conn->query($current_user_id_sql);

    
    foreach ($result as $key => $value) {
        $current_user_id = $value['id'];
    }

    foreach ($store_id as $key => $value) {
        $attach_sql = "INSERT INTO users_stores (user_id, store_id)
        VALUES ('$current_user_id', '$value')";

        if ($conn->query($attach_sql) === TRUE) {
            $flashMsg["message"] = "<span class='font-bold uppercase'>user</span> Added succesfully";
            $flashMsg["color"] = "bg-green-200 text-green-700";
        } else {
            $msg = $conn->error;
            $flashMsg["message"] =  $msg;
            $flashMsg["color"] = "bg-red-200 text-red-700";
        }

    }

    $_SESSION['flash_message_data'] = $flashMsg;
    header("Location:list_user.php");

}else{
    header("Location:form_user.php");
}

?>