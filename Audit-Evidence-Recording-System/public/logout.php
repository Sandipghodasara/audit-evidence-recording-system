<?php
session_start();

if( isset($_SESSION["userName"])){
    session_destroy();
    session_unset();
}

header("location:index.php");

?>