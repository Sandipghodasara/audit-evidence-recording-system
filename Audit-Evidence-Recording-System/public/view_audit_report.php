<?php
@session_start();
header('Content-type: application/json');

require_once "dbConnection.php";
if ($_POST && $_POST['method'] == 'store') {
    $current_store_id = $_POST['store_id'];
    $_SESSION['store_id'] = $current_store_id;
    $current_user_id = $_SESSION['user_id'];
    $res['status'] = true;
    $res['action'] = 'store';
}

if ($_POST && $_POST['method'] == 'filter') {
    $start_date = !empty($_POST['startDate']) ? $_POST['startDate'] : '';
    $end_date = !empty($_POST['endDate']) ? $_POST['endDate'] : '';

    $start_date = explode("-", $start_date);
    $start_date = $start_date[0] . "-" . $start_date[1] . "-" . $start_date[2];

    $end_date = explode("-", $end_date);
    $end_date = $end_date[0] . "-" . $end_date[1] . "-" . $end_date[2];


    $_SESSION['start_date'] = $start_date;
    $_SESSION['end_date'] = $end_date;
    $_SESSION['pdf_start_date'] = $start_date;
    $_SESSION['pdf_end_date'] = $end_date;
    $current_user_id = $_SESSION['user_id'];
    $res['status'] = true;
    $res['action'] = 'filter';
}

echo json_encode($res);
