<?php

session_start();

require_once "dbConnection.php";

function valid()
  {
    if (empty($_POST["store_name"])) {
        $flashMsg["message"][] = "Store name is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $store_name_valid = false;
    } else {
        $store_name_valid = true;
    }
    
    if (empty($_POST["email_address"])) {
        $flashMsg["message"][] = "Email is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $email_valid = false;
    } else {
        $email_valid = true;
    }

    $_SESSION['flash_message_data'] = $flashMsg;

    return $email_valid && $store_name_valid;
}

if ($_POST && valid() && isset($_POST['submit'])) {
    $current_store_id = $_POST['store_id'];
    $store_name = $_POST['store_name'];
    $email = $_POST['email_address'];
    $country = $_POST['country'];
    $street_address = $_POST['street_address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $postal_code = $_POST['postal_code'];
    $user_id = $_POST['selected'];

    $sql = "UPDATE stores SET   store_name='$store_name',
                                email='$email',
                                country='$country',
                                permanent_address='$street_address',
                                city='$city',
                                state='$state',
                                postal_code='$postal_code' WHERE id = $current_store_id";
    
    $flashMsg = [];
    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>store</span> record Updated succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }

    foreach ($user_id as $key => $value) {
        $attach_sql = "INSERT INTO users_stores (user_id, store_id)
        VALUES ('$value', '$current_store_id')";

        if ($conn->query($attach_sql) === TRUE) {
            $flashMsg["message"] = "<span class='font-bold uppercase'>user</span> Added succesfully";
            $flashMsg["color"] = "bg-green-200 text-green-700";
        } else {
            $msg = $conn->error;
            $flashMsg["message"] =  $msg;
            $flashMsg["color"] = "bg-red-200 text-red-700";
        }

        $_SESSION['flash_message_data'] = $flashMsg;
    }

    $_SESSION['flash_message_data'] = $flashMsg;
    header("Location:list_store.php");

}else{
    $current_store_id = $_POST['store_id'];
    header("Location:form_store.php?store_id=$current_store_id");
}





?>