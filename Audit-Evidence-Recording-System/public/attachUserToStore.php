<?php
session_start();

require_once "dbConnection.php";

if ($_POST && isset($_POST['attach'])) {
    $store_id = $_POST['selected'];
    $current_user_id = $_POST['user_id']; 
    
    $sql = "INSERT INTO users_stores (user_id, store_id)
    VALUES ('$current_user_id', '$store_id')";

    $flashMsg = [];
    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>store</span> Added succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }
}
$flashMsg["type"] = "Store";
$_SESSION['flash_message_data'] = $flashMsg;

$current_user_id = ($current_user_id)*987654321;
$current_user_id = urlencode(base64_encode($current_user_id));

header("Location:form_user.php?user_id=$current_user_id");

?>