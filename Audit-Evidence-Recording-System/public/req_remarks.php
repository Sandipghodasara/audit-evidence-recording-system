<?php

session_start();

require_once "dbConnection.php";

if (isset($_GET['remarks_id']) && isset($_GET['action'])) {
    $remark_id = $_GET['remarks_id'];
    $action = $_GET['action'];

    if ($action == 'approve') {
        $sql = "UPDATE remarks SET status = 0 WHERE id= $remark_id";
    
        if ($conn->query($sql) === TRUE) {
            $flashMsg["message"] = "<span class='font-bold uppercase'>Remarks</span> approved and added succesfully";
            $flashMsg["color"] = "bg-green-200 text-green-700";
        } else {
            $msg = $conn->error;
            $flashMsg["message"] =  $msg;
            $flashMsg["color"] = "bg-red-200 text-red-700";
        }
        $_SESSION['flash_message_data'] = $flashMsg;
        header("Location:list_remarks.php");
    }else{
        $sql = "UPDATE remarks SET status = 2 WHERE id= $remark_id";
    
        if ($conn->query($sql) === TRUE) {
            $flashMsg["message"] = "<span class='font-bold uppercase'>Remarks</span> rejected";
            $flashMsg["color"] = "bg-green-200 text-green-700";
        } else {
            $msg = $conn->error;
            $flashMsg["message"] =  $msg;
            $flashMsg["color"] = "bg-red-200 text-red-700";
        }
        $_SESSION['flash_message_data'] = $flashMsg;
        header("Location:list_remarks.php");
    }
    
}else{
    // header("Location:form_remarks.php");
}



?>