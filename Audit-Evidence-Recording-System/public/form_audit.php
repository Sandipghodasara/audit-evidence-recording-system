<?php
$page = "audit";
?>

<!-- Header section including sidebar -->
<?php
include "header.php";
?>

<?php
$current_audit_id = isset($_GET['audit_id']) ? $_GET['audit_id'] : null;
if (isset($_GET['audit_id'])) {

	$current_audit_id = base64_decode(urldecode($current_audit_id));
	$current_audit_id = $current_audit_id / 987654321;

	$action = isset($_GET['audit_id']) ? 'edit' : 'create';

	$sql = "select * from audit_remarks where id = $current_audit_id";
	$result = $conn->query($sql);

	if ($action === 'edit') {
		if ($result->num_rows > 0) {
			foreach ($result as $key => $value) {
				$date = $value['date'];
				$time = $value['time'];
				$img = $value['image'];
			}
		} else {
		}
	}
}
?>

<!-- inner content -->

<div class="md:max-w-6xl md:mx-auto px-4 pt-4">
	<?php
	include "flash_msg.php"
	?>
	<div class="mt-10 sm:mt-0">
		<div class="md:grid md:grid-cols-3 md:gap-6">
			<div class="mt-5 md:mt-0 md:col-span-4">
				<form action="./add_audit_remarks.php" method="POST" enctype="multipart/form-data">
					<div class="shadow overflow-hidden sm:rounded-md">
						<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
							<div class="px-4 flex bg-gray-50 sm:px-0">
								<h3 class="text-lg pr-2	font-medium leading-6 text-indigo-600">Audit</h3>
								<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 100 100" style=" fill:#000000;">
									<path fill="#78a0cf" d="M13 27A2 2 0 1 0 13 31A2 2 0 1 0 13 27Z"></path>
									<path fill="#f1bc19" d="M77 12A1 1 0 1 0 77 14A1 1 0 1 0 77 12Z"></path>
									<path fill="#cee1f4" d="M50 13A37 37 0 1 0 50 87A37 37 0 1 0 50 13Z"></path>
									<path fill="#f1bc19" d="M83 11A4 4 0 1 0 83 19A4 4 0 1 0 83 11Z"></path>
									<path fill="#78a0cf" d="M87 22A2 2 0 1 0 87 26A2 2 0 1 0 87 22Z"></path>
									<path fill="#fbcd59" d="M81 74A2 2 0 1 0 81 78 2 2 0 1 0 81 74zM15 59A4 4 0 1 0 15 67 4 4 0 1 0 15 59z"></path>
									<path fill="#78a0cf" d="M25 85A2 2 0 1 0 25 89A2 2 0 1 0 25 85Z"></path>
									<path fill="#fff" d="M18.5 51A2.5 2.5 0 1 0 18.5 56A2.5 2.5 0 1 0 18.5 51Z"></path>
									<path fill="#f1bc19" d="M21 66A1 1 0 1 0 21 68A1 1 0 1 0 21 66Z"></path>
									<path fill="#fff" d="M80 33A1 1 0 1 0 80 35A1 1 0 1 0 80 33Z"></path>
									<path fill="#fdfcee" d="M73.583,29.899c0.148-5.448-5.917-5.17-5.917-5.17l-31.44,0.084c-2.405,0-4.393,2.384-4.393,4.6 l-0.042,27.171l-6,0.012c0,1.645,0.022,13.473,0.042,13.683c0.404,4.354,5.331,3.947,5.331,3.947l36.086,0.037 c1.948,0.19,3-2.172,3-3.525s0-40.752,0-40.752L73.583,29.899z"></path>
									<path fill="#472b29" d="M67.47,75c-0.095,0.001-0.19-0.004-0.286-0.013l-36.021-0.033 c-1.893,0.147-5.656-0.599-6.027-4.605c-0.028-0.296-0.045-13.621-0.045-13.754c0-0.401,0.313-0.727,0.699-0.728l5.303-0.01 l0.041-26.446c0-2.514,2.178-5.327,5.093-5.327l31.438-0.083c0.108,0.002,3.256-0.126,5.185,1.847 c0.997,1.02,1.479,2.39,1.434,4.071c-0.01,0.387-0.31,0.697-0.682,0.707l-2.651,0.068v40.043c0,0.983-0.456,2.473-1.457,3.419 C68.907,74.712,68.212,75,67.47,75z M31.109,73.5l36.142,0.035c0.527,0.049,0.928-0.101,1.301-0.454 c0.649-0.613,0.999-1.665,0.999-2.343V29.985c0-0.395,0.303-0.718,0.683-0.728l2.63-0.068c-0.087-0.943-0.421-1.716-0.995-2.304 c-1.5-1.533-4.142-1.427-4.17-1.43L36.229,25.54c-2.048,0-3.695,2.118-3.695,3.873l-0.042,27.172 c-0.001,0.401-0.313,0.726-0.699,0.727l-5.301,0.01c0.003,3.077,0.023,12.505,0.04,12.913 C26.864,73.809,30.939,73.521,31.109,73.5z"></path>
									<path fill="#fef6aa" d="M32.708 57.058l-6.248-.016c-.005.208.003 11.963.023 12.15.28 3.897 5.121 3.371 6.225 3.35V57.058zM47.5 45.5L47.5 55.5 36.5 55.5 36.5 43.5 45.5 43.5"></path>
									<path fill="#472b29" d="M47.75,55.75h-11.5v-12.5h9.25c0.138,0,0.25,0.112,0.25,0.25s-0.112,0.25-0.25,0.25h-8.75v11.5h10.5 V45.5c0-0.138,0.112-0.25,0.25-0.25s0.25,0.112,0.25,0.25V55.75z"></path>
									<path fill="#fef6aa" d="M54.5 67.5L54.5 58.5 65.5 58.5 65.5 68.5 57.5 68.5"></path>
									<path fill="#472b29" d="M65.75 68.75H57.5c-.138 0-.25-.112-.25-.25s.112-.25.25-.25h7.75v-9.5h-10.5v8.75c0 .138-.112.25-.25.25s-.25-.112-.25-.25v-9.25h11.5V68.75zM29.791 70.7c-.348 0-.649-.259-.694-.613-.048-.384.224-.733.608-.781.905-.113 1.429-.404 1.429-1.556V54.917c0-.387.313-.7.7-.7s.7.313.7.7V67.75c0 1.191-.461 2.67-2.655 2.944C29.849 70.698 29.819 70.7 29.791 70.7zM70.5 31.533H51.25c-.387 0-.7-.313-.7-.7s.313-.7.7-.7H70.5c.387 0 .7.313.7.7S70.887 31.533 70.5 31.533z"></path>
									<g>
										<path fill="#472b29" d="M47.5,35.917H36.583c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25H47.5 c0.138,0,0.25,0.112,0.25,0.25S47.638,35.917,47.5,35.917z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M43.5,39.75h-7c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h7c0.138,0,0.25,0.112,0.25,0.25 S43.638,39.75,43.5,39.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M57.5,35.917h-6.917c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25H57.5 c0.138,0,0.25,0.112,0.25,0.25S57.638,35.917,57.5,35.917z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M65.5,35.917h-5c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h5 c0.138,0,0.25,0.112,0.25,0.25S65.638,35.917,65.5,35.917z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M65.5,39.75h-20c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h20 c0.138,0,0.25,0.112,0.25,0.25S65.638,39.75,65.5,39.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M65.5,43.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S65.638,43.75,65.5,43.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M65.5,47.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S65.638,47.75,65.5,47.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M65.5,51.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S65.638,51.75,65.5,51.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M65.5,55.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S65.638,55.75,65.5,55.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M51.5,59.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S51.638,59.75,51.5,59.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M51.5,63.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S51.638,63.75,51.5,63.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M51.5,67.75h-15c-0.138,0-0.25-0.112-0.25-0.25s0.112-0.25,0.25-0.25h15 c0.138,0,0.25,0.112,0.25,0.25S51.638,67.75,51.5,67.75z"></path>
									</g>
									<g>
										<path fill="#472b29" d="M47.417,31.533h-5.5c-0.387,0-0.7-0.313-0.7-0.7s0.313-0.7,0.7-0.7h5.5c0.387,0,0.7,0.313,0.7,0.7 S47.803,31.533,47.417,31.533z"></path>
									</g>
								</svg>
							</div>
						</div>

						<div class="px-4 py-5 bg-white sm:p-6">
							<!-- <div class="grid grid-cols-6 gap-6">
								<div class="col-span-6 sm:col-span-4">
									<input type="hidden" name="store_id" value="1">
									<label for="selected" class="block text-sm font-medium text-gray-700">Select store</label>
									<select name="selected" id="selected" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
										<?php
										$sql = "select * from stores WHERE id not in (select store_id from users_stores where user_id = 1)";
										$result = $conn->query($sql);

										foreach ($result as $key => $value) {
										?>
											<option value="<?php echo $value['id'] ?> "><?php echo $value['store_name'] ?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div> -->

							<div class="px-1 py-5 bg-white sm:py-1">
								<div class="grid grid-cols-6 gap-6">
									<div class="col-span-6 sm:col-span-2">
										<label for="datepicker1" class="block text-sm font-medium text-gray-700">Date</label>
										<input type="text" name="datepicker1" id="datepicker1" data-toggle="datepicker1" value="<?php echo !empty($current_audit_id) ? $date : '' ?>" class="mt-2 p-2 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-transparent block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
									</div>

									<div class="col-span-6 sm:col-span-1 pt-7">
										<a id="set_date_button" class="cursor-pointer inline-flex justify-center p-1.5 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
											Current Date
										</a>
									</div>

									<div class="col-span-6 sm:col-span-2">
										<label for="set_time" class="block text-sm font-medium text-gray-700">Time</label>
										<input type="text" name="set_time" id="set_time" data-toggle="set_time" value="<?php echo !empty($current_audit_id) ? $time : '' ?>" class="mt-2 p-2 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-transparent block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
									</div>

									<div class="col-span-6 sm:col-span-1 pt-7">
										<a id="set_time_button" class="cursor-pointer inline-flex justify-center p-1.5 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
											Current Time
										</a>
									</div>

									<div class="col-span-6 sm:col-span-3">
										<label for="remarks" class="block text-sm font-medium text-gray-700">Remakrs</label>
										<select name="remarks" id="remarks" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
											<?php
											$sql = "select * from remarks where status = 0";
											$result = $conn->query($sql);

											foreach ($result as $key => $value) {
												echo $value;
											?>
												<option value="<?php echo $value['id'] ?> "><?php echo $value['remark'] ?></option>
											<?php
											}
											?>
											<option value="custom">Custom</option>
										</select>
									</div>

									<div class="col-span-6 sm:col-span-3 custom_remarks invisible">
										<label for="custom_remarks" class="block text-sm font-medium text-gray-700">Custom Remark</label>
										<input type="text" name="custom_remarks" id="custom_remarks" class="mt-2 p-2 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-transparent block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
									</div>

									<div class="col-span-6 sm:col-span-6">
										<label class="block text-sm font-medium text-gray-700">
											Upload photo
										</label>
										<div class="bg-white p7 mt-2 rounded w-full mx-auto">
											<div x-data="dataFileDnD()" class=" img_template relative flex flex-col p-4 text-gray-400 border border-gray-200 rounded">
												<div x-ref="dnd" class="relative flex flex-col text-gray-400 border border-gray-200 border-dashed rounded cursor-pointer">
													<input accept="*" id="myupload" type="file" name="filename" multiple="" class="absolute inset-0 z-50 w-full h-full p-0 m-0 outline-none opacity-0 cursor-pointer" @change="addFiles($event)" @dragover="$refs.dnd.classList.add('border-blue-400'); $refs.dnd.classList.add('ring-4'); $refs.dnd.classList.add('ring-inset');" @dragleave="$refs.dnd.classList.remove('border-blue-400'); $refs.dnd.classList.remove('ring-4'); $refs.dnd.classList.remove('ring-inset');" @drop="$refs.dnd.classList.remove('border-blue-400'); $refs.dnd.classList.remove('ring-4'); $refs.dnd.classList.remove('ring-inset');" title="">

													<div class="flex flex-col items-center justify-center py-10 text-center">
														<svg class="w-6 h-6 mr-1 text-current-50" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
															<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
														</svg>
														<p class="m-0">Drag your files here or click in this area.</p>
													</div>
												</div>

												<template x-if="files.length > 0" class="">
													<div class="grid grid-cols-2 gap-4 mt-4 md:grid-cols-6" @drop.prevent="drop($event)" @dragover.prevent="$event.dataTransfer.dropEffect = 'move'">
														<template x-for="(_, index) in Array.from({ length: files.length })">
															<div class="relative flex flex-col items-center overflow-hidden text-center bg-gray-100 border rounded cursor-move select-none" style="padding-top: 100%;" @dragstart="dragstart($event)" @dragend="fileDragging = null" :class="{'border-blue-600': fileDragging == index}" draggable="true" :data-index="index">
																<button class="absolute top-0 right-0 z-50 p-1 bg-white rounded-bl focus:outline-none" type="button" @click="remove(index)">
																	<svg class="w-4 h-4 text-gray-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
																		<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
																	</svg>
																</button>
																<template x-if="files[index].type.includes('audio/')">
																	<svg class="absolute w-12 h-12 text-gray-400 transform top-1/2 -translate-y-2/3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
																		<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 19V6l12-3v13M9 19c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zm12-3c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zM9 10l12-3"></path>
																	</svg>
																</template>
																<template x-if="files[index].type.includes('application/') || files[index].type === ''">
																	<svg class="absolute w-12 h-12 text-gray-400 transform top-1/2 -translate-y-2/3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
																		<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
																	</svg>
																</template>
																<template x-if="files[index].type.includes('image/')">
																	<img class="absolute inset-0 z-0 object-cover w-full h-full border-4 border-white preview" x-bind:src="loadFile(files[index])">
																</template>
																<template x-if="files[index].type.includes('video/')">
																	<video class="absolute inset-0 object-cover w-full h-full border-4 border-white pointer-events-none preview">
																		<filedragging x-bind:src="loadFile(files[index])" type="video/mp4">
																		</filedragging>
																	</video>
																</template>

																<div class="absolute bottom-0 left-0 right-0 flex flex-col p-2 text-xs bg-white bg-opacity-50">
																	<span class="w-full font-bold text-gray-900 truncate" x-text="files[index].name">Loading</span>
																	<span class="text-xs text-gray-900" x-text="humanFileSize(files[index].size)">...</span>
																</div>

																<div class="absolute inset-0 z-40 transition-colors duration-300" @dragenter="dragenter($event)" @dragleave="fileDropping = null" :class="{'bg-blue-200 bg-opacity-80': fileDropping == index &amp;&amp; fileDragging != index}">
																</div>
															</div>
														</template>
													</div>
												</template>

												<?php
												if( isset($_GET['audit_id'])){
													?>
													<div class="img_clone">
													<div class="grid grid-cols-2 gap-4 mt-4 md:grid-cols-6">
														<div>
															<div class="relative flex flex-col items-center overflow-hidden text-center bg-gray-100 border rounded cursor-move select-none" style="padding-top: 100%;">
																<button class="absolute top-0 right-0 z-50 p-1 bg-white rounded-bl focus:outline-none btn_delete" type="button">
																	<svg class="w-4 h-4 text-gray-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
																		<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
																	</svg>
																</button>
																<div>
																	<img src="<?php echo '../uploads/'.$img ?>" class="absolute inset-0 z-0 object-cover w-full h-full border-4 border-white preview">
																</div>
																<div class="absolute bottom-0 left-0 right-0 flex flex-col p-2 text-xs bg-white bg-opacity-50">
																	<!-- <span class="text-xs text-gray-900 file_size">...</span> -->
																</div>

															</div>
														</div>
													</div>
												</div>
												<?php
												}
												?>

												<div class="img_clone hidden">
													<div class="grid grid-cols-2 gap-4 mt-4 md:grid-cols-6">
														<div>
															<div class="relative flex flex-col items-center overflow-hidden text-center bg-gray-100 border rounded cursor-move select-none" style="padding-top: 100%;">
																<button class="absolute top-0 right-0 z-50 p-1 bg-white rounded-bl focus:outline-none btn_delete" type="button">
																	<svg class="w-4 h-4 text-gray-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
																		<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
																	</svg>
																</button>
																<div>
																	<img src="" class="absolute inset-0 z-0 object-cover w-full h-full border-4 border-white preview">
																</div>
																<div class="absolute bottom-0 left-0 right-0 flex flex-col p-2 text-xs bg-white bg-opacity-50">
																	<span class="w-full font-bold text-gray-900 truncate file_name">Loading</span>
																	<span class="text-xs text-gray-900 file_size">...</span>
																</div>

															</div>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
							<button type="submit" name="submit" class="inline-flex justify-center p-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
								Save
							</button>
						</div>
				</form>
			</div>
		</div>
	</div>
</div>



<!-- footer section -->
<?php
include "footer.php"
?>