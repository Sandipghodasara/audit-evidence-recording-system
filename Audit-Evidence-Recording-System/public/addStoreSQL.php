<?php

session_start();

require_once "dbConnection.php";

function valid()
{
    if (empty($_POST["store_name"])) {
        $flashMsg["message"][] = "Store name is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $store_name_valid = false;
    } else {
        $store_name_valid = true;
    }

    if (empty($_POST["email_address"])) {
        $flashMsg["message"][] = "Email is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $email_valid = false;
    } else {
        $email_valid = true;
    }

    $_SESSION['flash_message_data'] = ' ';

    return $email_valid && $store_name_valid;
}


if ($_POST && valid() && isset($_POST['submit'])) {
    $store_name = $_POST['store_name'];
    $email = $_POST['email_address'];
    $country = $_POST['country'];
    $street_address = $_POST['street_address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $postal_code = $_POST['postal_code'];
    $user_id = $_POST['selected'];

    $sql = "INSERT INTO stores (store_name, email,country, permanent_address, city, state, postal_code)
    VALUES ('$store_name', '$email', '$country', '$street_address', '$city', '$state', '$postal_code')";

    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>store</span> record created succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }

    $current_store_id_sql = "SELECT id FROM `stores` WHERE store_name = '$store_name' AND email= '$email'";
    $result = $conn->query($current_store_id_sql);


    foreach ($result as $key => $value) {
        $current_store_id = $value['id'];
    }

    foreach ($user_id as $key => $value) {
        $attach_sql = "INSERT INTO users_stores (user_id, store_id)
        VALUES ('$value', '$current_store_id')";

        if ($conn->query($attach_sql) === TRUE) {
            $flashMsg["message"] = "<span class='font-bold uppercase'>user</span> Added succesfully";
            $flashMsg["color"] = "bg-green-200 text-green-700";
        } else {
            $msg = $conn->error;
            $flashMsg["message"] =  $msg;
            $flashMsg["color"] = "bg-red-200 text-red-700";
        }

        $_SESSION['flash_message_data'] = $flashMsg;
    }
    header("Location:list_store.php");
} else {
    header("location:form_store.php");
}
