<?php


session_start();

require "dbConnection.php";
$flashMsg = [];

function valid(){
    if (empty($_POST["datepicker1"])) {
        $flashMsg["message"][] = "Date is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $date_valid = false;
    } else {
        $date_valid = true;
    }

    if (empty($_POST["set_time"])) {
        $flashMsg["message"][] = "Time is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $time_valid = false;
    } else {
        $time_valid = true;
    }

    if (empty($_FILES["filename"]['name'])) {
        $flashMsg["message"][] = "File is required";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $file_valid = false;
    } else {
        $file_valid = true;
    }

    if (strtolower($_POST['custom_remarks']) == 'custom') {
        $flashMsg["message"][] = "You can't write custom";
        $flashMsg["color"] = "bg-red-200 text-red-700";
        $custom_valid = false;
    } else {
        $custom_valid = true;
    }

    $_SESSION['flash_message_data'] = $flashMsg;

    return $date_valid && $time_valid && $file_valid && $custom_valid;
}

if ($_POST && valid() && isset($_POST['submit'])) {
    $current_user_id = $_SESSION['user_id'];
    $date = $_POST['datepicker1'];

    $date = explode("-", $date);
    $date = $date[2]."-".$date[1]."-".$date[0];

    $time = $_POST['set_time'];
    $remark_id = $_POST['remarks'];
    $custom_remarks = $_POST['custom_remarks'];

    if (empty($_SESSION['store_id'])) {
        $first_time_get_store = "SELECT * FROM `users_stores` WHERE user_id = $current_user_id  ORDER BY store_id ASC LIMIT 1";
        $temp_store_id = $conn->query($first_time_get_store);
    
        foreach ($temp_store_id as $key => $value) {
            $current_store_id = $value['store_id'];
        }
    }else{
        $current_store_id =  $_SESSION['store_id'];
    }

    if (!empty($_FILES["filename"]["name"])) {
        // Get file info
        $targetDir = "../uploads/";
        $imgContent = basename($_FILES["filename"]["name"]);
        $targetFilePath = $targetDir . $imgContent;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);


        // Allow certain file formats 
        $allowTypes = array('jpg', 'png', 'jpeg');
        if (in_array($fileType, $allowTypes)) {
            $path = $_FILES['filename']['tmp_name'];
            if (move_uploaded_file($path, $targetFilePath)) {
                // Insert image content into database
                if ($remark_id === 'custom') {
                   $sql = "INSERT INTO remarks (remark,status) VALUES ('$custom_remarks', 1)";
                    $conn->query($sql);
                }

                $find_custom_remarks_id = "SELECT * FROM remarks WHERE remark = '$custom_remarks'";
                $result =  $conn->query($find_custom_remarks_id);

                foreach ($result as $key => $value) {
                    $remark_id = $value['id'];
                }

                $audit_remarks_sql = "INSERT into audit_remarks (store_id,user_id,date,time,remark_id,image) 
                VALUES ('$current_store_id', '$current_user_id', '$date', '$time', '$remark_id', '$imgContent')";

                if ($conn->query($audit_remarks_sql)  === TRUE) {
                    $flashMsg["message"] = "<span class='font-bold uppercase'>audit</span> created succesfully";
                    $flashMsg["color"] = "bg-green-200 text-green-700";
                } else {
                    $msg = $conn->error;
                    $flashMsg["message"] =  $msg;
                    $flashMsg["color"] = "bg-red-200 text-red-700";
                }

                $_SESSION['flash_message_data'] = $flashMsg;
                if ($_SESSION['role'] == 1) {
                    header("Location:record.php");
                }else{
                    header("Location:dashbord_user.php");
                }
            }
        } else {
            // header("location:dashbord_user.php");
            // $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        }
    } else {
        header("location:dashbord_user.php");
    }
} else {
    header("Location:form_audit.php");
}
