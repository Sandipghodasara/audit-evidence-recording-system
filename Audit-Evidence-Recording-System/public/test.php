<?php
ob_start();
// header('Content-type: application/pdf');
?>

<?php
require '../vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;

$options = new Options();
$options->set('isRemoteEnabled', TRUE);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        /* @page {
            margin: 0px;
        } */

        @page :first {
            margin-left: 0%;
            margin-top: 0%;
            margin-right: 0%;
            margin-bottom: 0%;
        }

        .header,
        .footer {
            position: fixed;
        }

        header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            /* background-color: lightblue; */
            height: 50px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            /* background-color: lightblue; */
            height: 50px;
        }

        h2,
        h3 {
            margin: 10px;
        }

        section {
            background: url(../src/images/main_page.jpg);
            background-size: cover;
            background-repeat: no-repeat;
        }

        table {
            border-collapse: collapse;
            padding: 5px auto;
        }

        tr,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        td {
            padding-left: 5px;
        }


        th {
            padding: auto 5px;
            width: 60px;
            text-align: left;
        }

        td {
            padding: auto 5px;
            width: 270px;
        }

        input {
            margin: 5px 0;
            width: 600px;
            border-top: 0;
            border-right: 0;
            border-left: 0;
        }
    </style>
</head>

<body>
    <!-- <table>
                    <tr>
                        <img src="../src/images/main page.jpg" alt="" width="100%">
                    </tr>
                </table> -->

    <?php

    session_start();
    require_once "dbConnection.php";

    $current_user_id = $_SESSION['user_id'];

    $first_time_get_store = "SELECT * FROM `users_stores` WHERE user_id = $current_user_id ORDER BY store_id ASC LIMIT 1";
    $temp_store_id = $conn->query($first_time_get_store);

    foreach ($temp_store_id as $key => $value) {
        $store_id = $value['store_id'];
    }

    $current_store_id = isset($_SESSION['store_id']) ? $_SESSION['store_id'] : $store_id;
    $start_date = isset($_SESSION['pdf_start_date']) ? $_SESSION['pdf_start_date'] : '';
    $end_date = isset($_SESSION['pdf_end_date']) ? $_SESSION['pdf_end_date'] : '';
    // unset($_SESSION['store_id']);


    if (empty($start_date) && empty($end_date)) {
        $sql = "SELECT * FROM `audit_remarks` JOIN remarks on remarks.id = audit_remarks.remark_id WHERE user_id=$current_user_id AND store_id=$current_store_id";
    } else {
        // "SELECT * FROM `audit_remarks` JOIN remarks on remarks.id = audit_remarks.remark_id WHERE audit_remarks.store_id=2 AND audit_remarks.user_id=4 AND date BETWEEN '2021-05-05' AND '2021-05-13'"
        $sql = "SELECT * FROM `audit_remarks` JOIN remarks on remarks.id = audit_remarks.remark_id WHERE user_id=$current_user_id AND store_id=$current_store_id AND date BETWEEN '$start_date' AND '$end_date'";
    }

    // $sql = "SELECT * FROM `audit_remarks` JOIN remarks on remarks.id = audit_remarks.remark_id WHERE user_id=$current_user_id AND store_id=$current_store_id";
    $result = $conn->query($sql);

    ?>

    <section style="height: 1122px; width:auto">
        <table style="position: absolute; top:825px; left:127px; border:none !important; border-style:hidden;">
            <tr>
                <th style="font-size: 25px; color: #02489f">
                    <?php

                    $sql_store_name = "SELECT * FROM stores where id = $current_store_id";
                    $result1 = $conn->query($sql_store_name);

                    foreach ($result1 as $key => $value) {
                        // echo $value['store_name']."<br>";
                    ?>
                        <h2 style="font-size:25px !important; margin:0; color:#02489f !important"> <?php echo $value['store_name'] ?> </h2>
                        <h4 style="font-size:15px !important; margin:1% 0 0 0; color:black !important">
                            <?php
                            if (empty($start_date) && empty($end_date)) {
                                echo '( Full Report )';
                            } else {
                                echo "( " . $start_date . "   TO   " . $end_date . " )";
                            }
                            ?>
                        </h4>
                    <?php
                    }
                    ?>
                </th>
            </tr>
        </table>
        <!-- <div style="position: absolute; top:830px; left:130px; border:none !important">hello</div> -->
    </section>

    <header style="text-align: right; padding-top:7px; text-decoration:underline">
    <h2>
        Auditing Report
    </h2>
    </header>
    <?php
    foreach ($result as $key => $record) {
        $id = $record['id'];
        $imageURL = '../uploads/' . $record["image"];
    ?>
        <div style="padding-top:2.5%; padding-bottom:2.5%">
            <table>
                <tr>
                    <th>Date</th>
                    <td><?php echo $record['date'] ?></td>
                    <th>Time</th>
                    <td><?php echo $record['time'] ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <img src="<?php echo $imageURL; ?>" alt="" width="100%">
                </tr>
            </table>
            <table>
                <tr>
                    <th>Remarks</th>
                    <td style="width: 620px !important;"><?php echo $record['remark'] ?></td>
                </tr>
            </table>
        </div>
    <?php
        // header('Content-Type: image/png');

    }
    ?>
    <footer style="padding-bottom: 7px;">
        <a href="https://www.globalvigilance.solutions/">www.globalvigilance.solutions</a>
    </footer>
</body>

</html>


<?php


$dompdf = new Dompdf($options);
$dompdf->load_html(ob_get_clean());
$dompdf->set_paper('A4', 'portrait');
$dompdf->render();
$dompdf->stream("test", array("Attachment" => 0));
?>