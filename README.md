# Audit Evidence Recording System #

This repository contains all the files for project *Audit Evidence Recording System* with sql file and all required plugin files.


### How do set up ###

* Clone or download the repository into your localhost( If using xampp then clone it in the folder named "htdocs")
* Import the sql file in the phpmyadmin database in the localhost.
* No need to download dev-dependencies
* Setup the connection.php file as per your database name
