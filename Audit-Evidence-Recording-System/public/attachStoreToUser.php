<?php
session_start();

require_once "dbConnection.php";

if ($_POST && isset($_POST['attach'])) {
    $user_id = $_POST['selected'];
    $current_store_id = $_POST['store_id']; 

    $sql = "INSERT INTO users_stores (user_id, store_id)
    VALUES ('$user_id', '$current_store_id')";

    $flashMsg = [];
    if ($conn->query($sql) === TRUE) {
        $flashMsg["message"] = "<span class='font-bold uppercase'>user</span> Added succesfully";
        $flashMsg["color"] = "bg-green-200 text-green-700";
    } else {
        $msg = $conn->error;
        $flashMsg["message"] =  $msg;
        $flashMsg["color"] = "bg-red-200 text-red-700";
    }
}
$_SESSION['flash_message_data'] = $flashMsg;

$temp_id = ($current_store_id)*987654321;
$current_store_id = urlencode(base64_encode($temp_id));


header("Location:form_store.php?store_id=$current_store_id");

?>