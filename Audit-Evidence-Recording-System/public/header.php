<?php
session_start();
include "dbConnection.php";
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script type="module" src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v1.9.8/dist/alpine.js" defer=""></script>
    <script src="../jquery-timepicker/jquery.timepicker.min.js" defer=""></script>
    <script src="../js/datepicker/dist/jquery.min.js"></script>
    <script src="https://unpkg.com/create-file-list"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="./customdatatable.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <link href="../js/datepicker/dist/datepicker.css" rel="stylesheet">
    <link href="../jquery-timepicker/jquery.timepicker.min.css" rel="stylesheet">
    <script src="../js/datepicker/dist/datepicker.js"></script>
    <link rel="stylesheet" href="tailwind.css">
    <title>add store</title>
</head>

<body class="antialiased bg-gray-200">

    <div x-data="{ sidemenu: false }" class="h-screen flex overflow-hidden" @keydown.window.escape="sidemenu = false">

        <div class="md:hidden">
            <div @click="sidemenu = false" class="fixed inset-0 z-30 bg-gray-600 opacity-0 pointer-events-none transition-opacity ease-linear duration-300" :class="{'opacity-75 pointer-events-auto': sidemenu, 'opacity-0 pointer-events-none': !sidemenu}"></div>

            <!-- Small Screen Menu -->
            <div class="fixed inset-y-0 left-0 flex flex-col z-40 max-w-xs w-full bg-white transform ease-in-out duration-300 -translate-x-full" :class="{'translate-x-0': sidemenu, '-translate-x-full': !sidemenu}">

                <!-- Brand Logo / Name -->
                <div class="flex items-center px-6 py-3 h-16">
                    <div class="text-2xl font-bold tracking-tight text-gray-800">Dashing Admin.</div>
                </div>
                <!-- @end Brand Logo / Name -->

            </div>
            <!-- @end Small Screen Menu -->
        </div>

        <!-- Menu Above Medium Screen -->
        <?php
        include "sidebar.php";
        ?>

        <!-- @end Menu Above Medium Screen -->
        <div class="flex-1 flex-col relative z-0 overflow-y-auto">
            <div class="px-4 md:px-8 py-2 h-16 flex justify-between items-center shadow-sm bg-white">
                <div class="flex items-center 
                           <?php
                            if ($_SESSION['role'] == 1) {
                                if ($page == 'remarks' || $page == 'record') {
                                    echo "visible";
                                } else {
                                    echo "invisible";
                                }
                            } else {
                                echo "visible";
                            }
                            ?>">
                    <div class="grid grid-cols-6 w-80">
                        <div class="col-span-6 sm:col-span-6">
                            <input type="hidden" name="store_id" value="1">
                            <select name="aa" id="aa" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                <?php
                                $is_selected = isset($_SESSION['store_id']) ? $_SESSION['store_id'] : '';
                                $current_user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';

                                if ($_SESSION['role'] == 2) {
                                    $sql = "SELECT stores.id as id, stores.store_name as store_name FROM `stores` JOIN users_stores on stores.id = users_stores.store_id WHERE users_stores.user_id = $current_user_id ORDER by users_stores.store_id ASC";
                                } else {
                                    $sql = "SELECT stores.id as id, stores.store_name as store_name FROM `stores` ORDER BY id ASC";
                                }
                                $result = $conn->query($sql);

                                foreach ($result as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['id'] ?>" <?php echo $value['id'] == $is_selected ? 'selected' : ' '  ?>><?php echo $value['store_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="flex items-center">
                    <?php
                    if ($_SESSION['role'] == 1) {
                    ?>
                        <a href="./list_remarks.php" class="relative inline-block text-gray-500 p-2 rounded-full hover:text-blue-600 hover:bg-gray-200 cursor-pointer mr-4">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <rect x="0" y="0" width="24" height="24" stroke="none"></rect>
                                <path d="M10 5a2 2 0 0 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6"></path>
                                <path d="M9 17v1a3 3 0 0 0 6 0v-1"></path>
                            </svg>
                            <?php
                            $sql = "SELECT count(*) as count FROM remarks where status = 1";
                            $result = $conn->query($sql);

                            foreach ($result as $key => $value) {
                                if ($value['count'] > 0) {
                            ?>
                                    <span class="absolute top-2 right-3 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">
                                <?php
                                    $sql = "SELECT count(*) as count from remarks where status = 1";
                                    $result = $conn->query($sql);

                                    foreach ($result as $key => $value) {
                                        echo $value['count'];
                                    }
                                }
                            }
                                ?>
                                    </span>
                        </a>
                    <?php
                    }
                    ?>

                    <div class="relative" x-data="{ open: false }">
                        <div @click="open = !open" class="cursor-pointer uppercase font-bold w-10 h-10 bg-blue-200 text-blue-600 flex items-center justify-center rounded-full">
                            <?php
                            echo $_SESSION['userName'];
                            ?>
                        </div>

                        <div x-show.transition="open" @click.away="open = false" class="absolute top-0 mt-12 right-0 w-48 bg-white py-2 shadow-md border border-gray-100 rounded-lg z-40" style="display: none;">
                            <a href="./logout.php" class="block px-4 py-2 text-gray-600 hover:bg-gray-100 hover:text-blue-600">Log
                                Out</a>
                        </div>
                    </div>
                </div>
            </div>