<?php
$page = "dashbord";
?>

<div class="hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center" id="modal-id">
	<div class="relative w-auto my-6 mx-auto max-w-3xl">
		<!--content-->
		<div class="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
			<!--header-->
			<!--body-->
			<div class="relative p-6 flex-auto">
				<button id="btn_close" class="focus:outline-none cursor-pointer absolute top-0 bottom-0 right-0 hover:bg-gray-100 text-black w-10 h-10 rounded-full inline-flex items-center justify-center mt-2 mr-3" x-on:click="open = false" type="button" onclick="toggleModal('modal-id')">
					<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
						<rect x="0" y="0" width="24" height="24" stroke="none"></rect>
						<line x1="18" y1="6" x2="6" y2="18" />
						<line x1="6" y1="6" x2="18" y2="18" />
					</svg>
				</button>
				<img id="img_model" src="<?php echo isset($_GET['img_url']) ? $_GET['img_url'] : '' ?>" width="100%" alt="" srcset="">
			</div>
		</div>
	</div>
</div>
<div class="hidden opacity-25 fixed inset-0 z-40 bg-black" id="modal-id-backdrop"></div>
<script type="text/javascript">
	function toggleModal(modalID) {
		document.getElementById(modalID).classList.toggle("hidden");
		document.getElementById(modalID + "-backdrop").classList.toggle("hidden");
		document.getElementById(modalID).classList.toggle("flex");
		document.getElementById(modalID + "-backdrop").classList.toggle("flex");
	}
</script>

<!-- Header section including sidebar -->
<?php
include "header.php";
?>

<!-- inner content -->
<div class="md:max-w-6xl md:mx-auto px-4 pt-4">
	<?php
	include "flash_msg.php"
	?>
	<div class="mt-10 sm:mt-0">
		<div class="md:grid md:grid-cols-3 md:gap-6">
			<div class="mt-5 md:mt-0 md:col-span-4">
				<form action="./view_audit_report.php" method="POST">
					<div class="shadow overflow-hidden sm:rounded-md">
						<div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
							<div class="px-4 flex bg-gray-50 sm:px-0">
								<h3 class="text-lg pr-2	font-medium leading-6 text-indigo-600">Filter</h3>
								<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="25" height="25" viewBox="0 0 100 100" style=" fill:#000000;">
									<path fill="#9d9fce" d="M13 27A2 2 0 1 0 13 31A2 2 0 1 0 13 27Z"></path>
									<path fill="#f1bc19" d="M77 12A1 1 0 1 0 77 14A1 1 0 1 0 77 12Z"></path>
									<path fill="#e4e4f9" d="M50 13A37 37 0 1 0 50 87A37 37 0 1 0 50 13Z"></path>
									<path fill="#f1bc19" d="M83 11A4 4 0 1 0 83 19A4 4 0 1 0 83 11Z"></path>
									<path fill="#9d9fce" d="M87 22A2 2 0 1 0 87 26A2 2 0 1 0 87 22Z"></path>
									<path fill="#fbcd59" d="M81 74A2 2 0 1 0 81 78 2 2 0 1 0 81 74zM15 59A4 4 0 1 0 15 67 4 4 0 1 0 15 59z"></path>
									<path fill="#9d9fce" d="M25 85A2 2 0 1 0 25 89A2 2 0 1 0 25 85Z"></path>
									<path fill="#fff" d="M18.5 51A2.5 2.5 0 1 0 18.5 56A2.5 2.5 0 1 0 18.5 51Z"></path>
									<path fill="#f1bc19" d="M21 66A1 1 0 1 0 21 68A1 1 0 1 0 21 66Z"></path>
									<path fill="#fff" d="M80 33A1 1 0 1 0 80 35A1 1 0 1 0 80 33Z"></path>
									<g>
										<path fill="#c0d078" d="M65,28c3.86,0,7,3.14,7,7v30c0,3.86-3.14,7-7,7H35c-3.86,0-7-3.14-7-7V35c0-3.86,3.14-7,7-7H65"></path>
										<path fill="#472b29" d="M65,28.4c3.639,0,6.6,2.961,6.6,6.6v30c0,3.639-2.961,6.6-6.6,6.6H35c-3.639,0-6.6-2.961-6.6-6.6V35 c0-3.639,2.961-6.6,6.6-6.6H65 M65,27H35c-4.418,0-8,3.582-8,8v30c0,4.418,3.582,8,8,8h30c4.418,0,8-3.582,8-8V35 C73,30.582,69.418,27,65,27L65,27z"></path>
										<path fill="#fdfcee" d="M63,69H37c-3.309,0-6-2.691-6-6V37c0-3.309,2.691-6,6-6h26c3.309,0,6,2.691,6,6v26 C69,66.309,66.309,69,63,69z"></path>
										<path fill="#472b29" d="M63 69H37c-3.309 0-6-2.691-6-6V37c0-3.309 2.691-6 6-6h24.625C61.832 31 62 31.168 62 31.375s-.168.375-.375.375H37c-2.895 0-5.25 2.355-5.25 5.25v26c0 2.895 2.355 5.25 5.25 5.25h26c2.895 0 5.25-2.355 5.25-5.25V48.375c0-.207.168-.375.375-.375S69 48.168 69 48.375V63C69 66.309 66.309 69 63 69zM68.625 42c-.207 0-.375-.168-.375-.375v-1.278c0-.207.168-.375.375-.375S69 40.14 69 40.347v1.278C69 41.832 68.832 42 68.625 42z"></path>
										<path fill="#472b29" d="M68.625,47c-0.207,0-0.375-0.168-0.375-0.375v-3.25c0-0.207,0.168-0.375,0.375-0.375 S69,43.168,69,43.375v3.25C69,46.832,68.832,47,68.625,47z"></path>
									</g>
									<g>
										<path fill="#e1e0d8" d="M54 37.375A8.625 8.625 0 1 0 54 54.625A8.625 8.625 0 1 0 54 37.375Z"></path>
										<path fill="#a8dbdb" d="M54 39.25A6.75 6.75 0 1 0 54 52.75A6.75 6.75 0 1 0 54 39.25Z"></path>
										<path fill="#88ae45" d="M46.039,53.961c-0.781-0.781-2.047-0.781-2.828,0c-0.563,0.563-4.686,4.686-5.25,5.25 c-0.781,0.781-0.781,2.047,0,2.828c0.781,0.781,2.047,0.781,2.828,0c0.564-0.564,4.687-4.687,5.25-5.25 C46.82,56.008,46.82,54.742,46.039,53.961z"></path>
										<path fill="#d1dc82" d="M46.039,56.789c0.781-0.781,0.781-2.047,0-2.828c-0.781-0.781-2.047-0.781-2.828,0l0,0 L46.039,56.789z"></path>
										<g>
											<path fill="#472b29" d="M54,55c-4.962,0-9-4.038-9-9s4.038-9,9-9s9,4.038,9,9S58.962,55,54,55z M54,37.75 c-4.549,0-8.25,3.701-8.25,8.25s3.701,8.25,8.25,8.25s8.25-3.701,8.25-8.25S58.549,37.75,54,37.75z"></path>
											<path fill="#472b29" d="M54,53c-3.86,0-7-3.14-7-7s3.14-7,7-7s7,3.14,7,7S57.86,53,54,53z M54,39.5 c-3.584,0-6.5,2.916-6.5,6.5s2.916,6.5,6.5,6.5s6.5-2.916,6.5-6.5S57.584,39.5,54,39.5z"></path>
											<path fill="#472b29" d="M45.801 52.484H48.231V53.484H45.801z" transform="rotate(-45.001 47.015 52.985)"></path>
											<path fill="#472b29" d="M39.375,63c-0.634,0-1.231-0.247-1.679-0.696C37.247,61.856,37,61.259,37,60.625 s0.247-1.231,0.696-1.679l5.25-5.25c0.896-0.897,2.461-0.897,3.358,0l0,0l0,0l0,0C46.753,54.145,47,54.741,47,55.375 s-0.247,1.231-0.696,1.679l-5.25,5.25C40.606,62.753,40.009,63,39.375,63z M44.625,53.75c-0.434,0-0.842,0.169-1.149,0.476 l-5.25,5.25c-0.307,0.307-0.476,0.715-0.476,1.149s0.169,0.842,0.476,1.149c0.614,0.614,1.684,0.614,2.298,0l5.25-5.25 c0.307-0.307,0.476-0.715,0.476-1.149s-0.169-0.842-0.476-1.149l0,0C45.467,53.919,45.059,53.75,44.625,53.75z"></path>
											<path fill="#472b29" d="M44.301 53.498H45.051V57.354H44.301z" transform="rotate(-45.001 44.675 55.427)"></path>
											<path fill="#472b29" d="M40.5 60c-.053 0-.103-.034-.119-.087l-.75-2.375c-.021-.066.016-.136.082-.157.068-.019.136.016.157.082l.75 2.375c.021.066-.016.136-.082.157C40.525 59.998 40.512 60 40.5 60zM40 61c-.053 0-.103-.034-.119-.087l-.943-2.986c-.021-.066.016-.136.082-.157.068-.02.137.016.157.082l.943 2.986c.021.066-.016.136-.082.157C40.025 60.998 40.012 61 40 61zM39.696 62.622c-.053 0-.103-.034-.119-.087l-1.258-3.985c-.021-.066.016-.136.082-.157.069-.021.136.017.157.082l1.258 3.985c.021.066-.016.136-.082.157C39.721 62.62 39.708 62.622 39.696 62.622zM38.921 62.75c-.053 0-.103-.034-.119-.087l-1.115-3.531c-.021-.066.016-.136.082-.157.07-.019.137.017.157.082l1.115 3.531c.021.066-.016.136-.082.157C38.946 62.748 38.933 62.75 38.921 62.75z"></path>
										</g>
									</g>
								</svg>
							</div>
						</div>

						<div class="px-4 py-5 bg-white sm:p-6">
							<input type="hidden" value="<?php echo isset($_SESSION['store_id']) ? $_SESSION['store_id'] : '' ?>" id="store_id" name="store_id">
							<div class="grid grid-cols-6 gap-6">
								<div class="col-span-6 sm:col-span-2">
									<label for="startDate" class="block text-sm font-medium text-gray-700">Start date</label>
									<input type="text" name="startDate" id="startDate" data-toggle="startDate" value="" class="mt-2 p-2 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-transparent block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
								</div>

								<div class="col-span-6 sm:col-span-2">
									<label for="endDate" class="block text-sm font-medium text-gray-700">End date</label>
									<input type="text" name="endDate" id="endDate" data-toggle="endDate" value="" class="mt-2 p-2 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-transparent block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
								</div>

								<div class="col-span-6 sm:col-span-2 flex justify-end">
									<button type="button" id="btn_filter" class="mt-7 mr-4 cursor-pointer shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-8 rounded-lg">
										Filter
									</button>
									<a href="./test.php" id="btn_view" class="mt-7 cursor-pointer shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
										View report
									</a>
								</div>
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="md:max-w-6xl md:mx-auto pt-4">
	<div class="overflow-x-auto bg-white rounded-lg shadow">
		<table id="myTable" class="w-full whitespace-no-wrap bg-white overflow-hidden table-striped">
			<thead>
				<tr class="text-left">
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">ID</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Date</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Time</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Remarks</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Images</th>
					<th class="px-6 py-3 text-gray-500 font-bold tracking-wider uppercase text-xs">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$current_user_id = $_SESSION['user_id'];
				$first_time_get_store = "SELECT * FROM `users_stores` WHERE user_id = $current_user_id  ORDER BY store_id ASC LIMIT 1";
				$temp_store_id = $conn->query($first_time_get_store);

				foreach ($temp_store_id as $key => $value) {
					$store_id = $value['store_id'];
				}

				$current_user_id = $_SESSION['user_id'];
				$current_store_id = isset($_SESSION['store_id']) ? $_SESSION['store_id'] : $store_id;
				$start_date = isset($_SESSION['start_date']) ? $_SESSION['start_date'] : '';
				$end_date = isset($_SESSION['end_date']) ? $_SESSION['end_date'] : '';
				// unset($_SESSION['store_id']);

				if (empty($start_date) && empty($end_date)) {
					$sql = "SELECT a.id as id, a.date as date, a.time as time, a.image as image, r.remark as remark FROM `audit_remarks` as a JOIN remarks as r on r.id = a.remark_id WHERE a.user_id=$current_user_id AND a.store_id=$current_store_id";
				} else {
					$sql = "SELECT * FROM `audit_remarks` JOIN remarks on remarks.id = audit_remarks.remark_id WHERE audit_remarks.user_id=$current_user_id AND audit_remarks.store_id=$current_store_id AND date BETWEEN '$start_date' AND '$end_date'";
				}

				// $sql = "SELECT * FROM `audit_remarks` JOIN remarks on remarks.id = audit_remarks.remark_id WHERE user_id=$current_user_id AND store_id=$current_store_id";
				$result = $conn->query($sql);

				foreach ($result as $key => $record) {
					$id = $record['id'];

					$temp_id = ($id) * 987654321;
					$encode_id = urlencode(base64_encode($temp_id));

					$date = explode("-", $record['date']);
					$date = $date[2] . "-" . $date[1] . "-" . $date[0];
				?>
					<tr class="focus-within:bg-gray-200 overflow-hidden">
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $key + 1 ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $date ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $record['time'] ?></span>
						</td>
						<td class="border-t">
							<span class="text-gray-700 px-6 py-4 flex items-center"><?php echo $record['remark'] ?></span>
						</td>
						
						<td class="border-t" width="15%">
							<button id="td" class="text-gray-700 focus:outline-none px-5 py-3 flex items-center" type="button" onclick="toggleModal('modal-id')">
								<img src="../uploads/<?php echo $record['image'] ?>" alt="">
							</button>
						</td>
						<td class="border-t">
						<a href="<?php echo "form_audit.php?audit_id=" . $encode_id ?>" class="shadow inline-flex items-center bg-blue-500 hover:bg-blue-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
                                Edit
                            </a>
                            <a href="<?php echo "delete_audit.php?audit_id=" . $encode_id ?>" class="shadow inline-flex items-center bg-red-500 hover:bg-red-600 focus:outline-none focus:shadow-outline text-sm text-white font-semibold py-1 px-4 rounded-lg">
                                Delete
                            </a>
						</td>
					</tr>
				<?php
				}
				unset($_SESSION['start_date']);
				unset($_SESSION['end_date']);
				?>
			</tbody>
		</table>
	</div>
</div>


<!-- footer section -->
<?php
include "footer.php"
?>